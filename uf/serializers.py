from rest_framework import serializers

class UfvalueSerializer(serializers.Serializer):
	id = serializers.IntegerField(read_only=True)
	date = serializers.DateTimeField(required=True)
	value = serializers.FloatField(required=True)

	def create(self, validated_data):
		return Snippet.objects.create(**validated_data)

	def update(self, instance, validated_data):
		instance.date = validated_data.get('date', instance.date)
		instance.value = validated_data.get('value', instance.value)
		instance.save()
		return instance

class UfConverterSerializer(serializers.Serializer):
	id = serializers.IntegerField(read_only=True)
	date_for_convertion = serializers.DateField(required=True)
	amount_in_uf = serializers.FloatField(required=True)
	uf_value_in_clp_for_given_date = serializers.FloatField(required=True)
	value_in_clp = serializers.FloatField(required=True)
	error_message = serializers.CharField(required=False, allow_blank=True, max_length=200)

	def create(self, validated_data):
		return Snippet.objects.create(**validated_data)



