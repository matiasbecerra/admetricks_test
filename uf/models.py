from django.db import models

class Ufvalue(models.Model):
    date = models.DateTimeField()
    value = models.FloatField()
