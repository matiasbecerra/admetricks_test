from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from uf.models import Ufvalue
from uf.serializers import UfvalueSerializer, UfConverterSerializer
from datetime import date as datefunction
from django.http import HttpResponse

@api_view(['GET'])
def list(request):
	if request.method == 'GET': #it inly handles GET requests
		ufvalues = Ufvalue.objects.all()
		serializer = UfvalueSerializer(ufvalues, many = True)
		return Response(serializer.data)
	else:
	  	return HttpResponse(status = 400)

@api_view(['GET'])
def price(request):
	#it only handles get requests
	if request.method == 'GET':
		validated_data = {}
		try:
			#trying to cast the amount in a float value
			amount_in_uf = float(request.GET.get('value','0'))
			validated_data['amount_in_uf'] = amount_in_uf
			date_value = get_date_for_convertion(request)
			validated_data['date_for_convertion'] = date_value
			validated_data['uf_value_in_clp_for_given_date'], validated_data['value_in_clp'] = get_values_in_clp(amount_in_uf, date_value)
			
		except Exception as e:
			#save the error for feedback
			validated_data['error_message'] = str(e)
			
		serializer = UfConverterSerializer(data = validated_data)
		if serializer.is_valid():
			return Response(serializer.data)
		return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

	else:
	  	return HttpResponse(status = 400)

def get_date_for_convertion(request):
	date = str(request.GET.get('date',''))
	#trying to cast the date to datetime.date
	year = int(date[0:4])
	month = int(date[4:6])
	day = int(date[6:8])
	date_value = datefunction(year = year, month = month, day = day)
	return date_value

def get_values_in_clp(amount_in_uf, date_value):
	uf_query_result = Ufvalue.objects.filter(date =  date_value)		
	if len(uf_query_result) > 0: #if the query to ufvalues works, then we calculate the conversion in clp
		uf_value_in_clp = uf_query_result[0].value
		clp = uf_value_in_clp*amount_in_uf
		return uf_value_in_clp, clp
