from django.conf.urls import url
from uf import views

urlpatterns = [
    url(r'^uf/list$', views.list),
    url(r'^uf/price$', views.price),
]