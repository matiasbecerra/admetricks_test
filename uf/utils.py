from bs4 import BeautifulSoup
from selenium import webdriver
from datetime import date
from uf.models import Ufvalue

months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']

def scraper():
	#first erase contents of ufvalue
	Ufvalue.objects.all().delete()
	#then star populating
	browser = webdriver.Firefox()
	browser.get("http://si3.bcentral.cl/Indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA%3d")	

	#iterates through years with data available
	for year in range(1977,2019):

		choose_year(browser, year)
		tbody = get_table_body(browser)
		for m, month in enumerate(months):
			for iterator in range(2, 33):
				span, span_name = get_span(tbody, iterator, month)
				save_span_value(span, span_name, year, m, iterator)

def choose_year(browser, year):
	#choose the desired year from the options
	element = browser.find_element_by_id('DrDwnFechas')
	for option in element.find_elements_by_tag_name('option'):
		if option.text == str(year):
			option.click()
			break

def get_table_body(browser):
	soup = BeautifulSoup(browser.page_source, "html.parser")
	html = list(soup.children)[0]
	body = list(html.children)[2]
	form = list(body.children)[1]
	table = soup.find('table', id = 'gr')
	tbody = list(table.children)[1]
	return tbody

def get_span(tbody, iterator, month):
	iterator_str = str(iterator).zfill(2)
	span_name = "gr_ctl" + iterator_str + '_' + month
	span = tbody.find('span', id = span_name)
	return span, span_name

def save_span_value(span, span_name, year, month, iterator):
	if span != None: 
		value = span.text
		if value != '':
			try:
				#if the data is consistent, save it
				day = int(iterator) - 1
				dateval = date(year, month + 1, day)
				ufval = Ufvalue(date = dateval, value = float(value.replace('.', '').replace(',', '.')))
				ufval.save()
				print(year, span_name, value)
			except Exception as e:
				#verbose on
				print(e)
				print(int(iterator) - 1, 1 + month)
				print(year, span_name, value)
				pass