Installation steps:

1: Download and install Python 3.6

2: Make a Virtual Environment with Python 3.6.
/usr/local/lib/python3.6.1/bin/python3 -m venv <location-of-virtualenv>

3: change to admetricks_test directory
cd /some/path/admetricks_test

4: Activate the virtual environment. 
source <location-of-virtualenv>/bin/activate

5: install django
pip install django

6: install django rest framework
pip install djangorestframework

7: Install selenium (only if you populate the database with the scraper)
pip install selenium

8: Download geckodriver from https://github.com/mozilla/geckodriver/releases and extract to any directory you like

9: Add geckodrive to the system path. (only if you populate the database with the scraper)
export PATH=$PATH:/path/to/geckodriver/folder/

10: Check if bs4 library is installed.
pip install bs4



To populate the database:

option 1: change the name of "db.sqlite3.database" to "db.sqlite3"

option 2: populate the database
	0: install mozilla firefox
	1: cd /some/path/admetricks_test
	2: prepare the database: 
			python manage.py makemigrations uf
	 		python manage.py migrate
	3: go to django shell: python manage.py shell
	4: in the django shell, execute:
			from uf.utils import scraper
			scraper()


To Consume The App:

1: cd /some/path/admetricks_test
2: python manage.py runserver
3: open a browser

	for listing the uf historical time serie: localhost:8000/uf/list

	for getting a conversion in CLP of a given amount in UF for an specific date: 
	localhost:8000/uf/price?value=XXXX&date=yyyymmdd

	*XXXX debe ser un numero entero o float con "."
	*yyyymmdd debe ser un entero de 8 dígitos que represente una fecha